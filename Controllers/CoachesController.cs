﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EFWebApiDemo.Models;
using EFWebApiDemo.Models.Domain;

namespace EFWebApiDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoachesController : ControllerBase
    {
        private readonly TrainingDbContext _context;

        public CoachesController(TrainingDbContext context)
        {
            _context = context;
        }

        // GET: api/Coaches
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Coach>>> GetCoaches()
        {
            return await _context.Coaches.ToListAsync();
        }

        // GET: api/Coaches/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Coach>> GetCoach(int id)
        {
            var coach = await _context.Coaches.FindAsync(id);

            if (coach == null)
            {
                return NotFound();
            }

            return coach;
        }

        // PUT: api/Coaches/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCoach(int id, Coach coach)
        {
            if (id != coach.Id)
            {
                return BadRequest();
            }

            _context.Entry(coach).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CoachExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Coaches
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Coach>> PostCoach(Coach coach)
        {
            _context.Coaches.Add(coach);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCoach", new { id = coach.Id }, coach);
        }

        // DELETE: api/Coaches/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Coach>> DeleteCoach(int id)
        {
            var coach = await _context.Coaches.FindAsync(id);
            if (coach == null)
            {
                return NotFound();
            }

            _context.Coaches.Remove(coach);
            await _context.SaveChangesAsync();

            return coach;
        }

        private bool CoachExists(int id)
        {
            return _context.Coaches.Any(e => e.Id == id);
        }

        // My added methods - you may see the value in services when looking at these and how they can bloat the controller. 

        // Get certifications for coach
        [HttpGet("{id}/certifications")]
        public async Task<ActionResult<IEnumerable<Certification>>> GetCertsForCoach(int id)
        {
            // Gets the coach by id, then takes that results and gets the certifications as a list.
            Coach coach = await _context.Coaches.Include(c => c.Certifications).FirstOrDefaultAsync(c => c.Id == id);
            // Because we have eagerly loaded Certification to coaches, each cert has a coach.
            // This will cause a circular infinite reference. Just place a break point at the start of this method
            // and go look at the content of coach, youll see it has certs, which have coaches and so on.
            // We need to stop it from doing that, a good option is to use DTOs to stop this circular reference.
            // We simply set the coaches to null for the certs in this because we are not using dtos, so we need 
            // to detach the coaches from certs.
            foreach(Certification cert in coach.Certifications)
            {
                // To avoid circular reference
                cert.Coaches = null;
            }

            return coach.Certifications.ToList();
        }

        // Add certifications to coach
        [HttpPost("{id}/certifications")]
        public async Task<IActionResult> AddCertsToCoach(int id, List<int> certifications)
        {
            // Because certification has a required name, we need to include it in the request body 
            // otherwise it will 400 us. We could get around this by passing a list of integers and fetching each cert with that id.

            // First we get the coach and their certifications
            Coach coach = await _context.Coaches.Include(c=>c.Certifications).FirstOrDefaultAsync(c=>c.Id == id);
            if (coach == null)
            {
                return NotFound();
            }
            foreach(int certId in certifications)
            {
                // Check if it exists, if not, add it
                if (coach.Certifications.FirstOrDefault(c => c.Id == certId) == null)
                {
                    // Go fetch the certification based on that id
                    Certification cert = await _context.Certifications.FindAsync(certId);
                    coach.Certifications.Add(cert);
                }
            }
            // Save changes to commit to db
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // Update certifications for coach
        [HttpPut("{id}/certifications")]
        public async Task<IActionResult> UpdateCertsForCoach(int id, List<int> certifications)
        {
            // Because certification has a required name, we need to include it in the request body 
            // otherwise it will 400 us. We could get around this by passing a list of integers and fetching each cert with that id.

            // First we get the coach and their certifications
            Coach coach = await _context.Coaches.Include(c => c.Certifications).FirstOrDefaultAsync(c => c.Id == id);
            if (coach == null)
            {
                return NotFound();
            }
            // This complete removal and adding is not always the ideal approach, a version with a check can be found at UpdateCertsForCoachv2() method

            // Remove old certifications
            coach.Certifications.Clear();

            // Create list of new certifications
            foreach (int certId in certifications)
            {
                Certification cert = await _context.Certifications.FindAsync(certId);
                coach.Certifications.Add(cert);
            }
            // Save changes to commit to db
            await _context.SaveChangesAsync();

            return NoContent();
        }
         
        // Update certifications for coach v2
        [HttpPut("{id}/certifications/v2")]
        public async Task<IActionResult> UpdateCertsForCoachv2(int id, List<int> newCerts)
        {
            // Because certification has a required name, we need to include it in the request body 
            // otherwise it will 400 us. We could get around this by passing a list of integers and fetching each cert with that id.

            // First we get the coach and their certifications
            Coach coach = await _context.Coaches.Include(c => c.Certifications).FirstOrDefaultAsync(c => c.Id == id);
            if (coach == null)
            {
                return NotFound();
            }

            // This method doesnt clear everything and re add, it checks to see if a cert needs to be added or removed first.
            List<Certification> oldCerts = coach.Certifications.ToList();
            foreach (Certification oldCert in oldCerts)
            {
                // Check if an existing certification is in the newCerts int array
                // If it isnt, we remove it from coach
                if (!newCerts.Contains(oldCert.Id))
                {
                    coach.Certifications.Remove(oldCert);
                }
            }
            // Add new certs that dont exists in the list.     
            foreach (int certId in newCerts)
            {
                if (!coach.Certifications.Select(c => c.Id).Contains(certId))
                {
                    Certification cert = await _context.Certifications.FindAsync(certId);
                    coach.Certifications.Add(cert);
                }           
            }
            // Save changes to commit to db
            await _context.SaveChangesAsync();

            return NoContent();
        }


    }
}
