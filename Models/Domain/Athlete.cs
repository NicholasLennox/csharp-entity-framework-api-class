﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EFWebApiDemo.Models.Domain
{
    public class Athlete
    {
        // PK
        public int Id { get; set; }
        // Fields
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        [MaxLength(15)]
        public string Gender { get; set; }
        public int Records { get; set; }
        // Relationships and navigations
        public int CoachId { get; set; }
        public Coach Coach { get; set; }
    }
}
