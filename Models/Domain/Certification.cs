﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EFWebApiDemo.Models.Domain
{
    public class Certification
    {
        // PK
        public int Id { get; set; }
        // Fields
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        // Relationships
        public ICollection<Coach> Coaches { get; set; }
    }
}
